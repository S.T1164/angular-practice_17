import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  message : string;
  myControl : FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.message = 'plese select button.';
    this.myControl = new FormGroup({
      name : new FormControl(''),
      age : new FormControl(0),
      email : new FormControl(''),
    });  
  }

  click(){
    this.message = JSON.stringify(this.myControl.value);
  }
  
}
